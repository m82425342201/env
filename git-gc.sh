for d in `ls -d *`; do
  if [ -d $d ]; then
    echo ==============
    echo $d
    echo ==============
    r=`pwd`
    cd $d
    git gc
    cd $r
  fi
done