cd /tmp
wget https://download.visualstudio.microsoft.com/download/pr/351400ef-f2e6-4ee7-9d1b-4c246231a065/9f7826270fb36ada1bdb9e14bc8b5123/dotnet-sdk-7.0.302-linux-x64.tar.gz
sudo mkdir -p /opt/dotnet
sudo tar xvf dotnet-sdk-7.0.302-linux-x64.tar.gz -C /opt/dotnet
sudo ln -s /opt/dotnet/dotnet /usr/local/bin
rm dotnet-sdk-7.0.302-linux-x64.tar.gz
