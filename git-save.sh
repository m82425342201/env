for d in `ls -d *`; do
  if [ -d $d ]; then
    echo ==============
    echo $d
    echo ==============
    r=`pwd`
    cd $d
    git pull
    git add .
    git commit -m ...
    git push
    cd $r
  fi
done