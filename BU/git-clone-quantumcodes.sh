sh /workspace/ssh-keys/quantumcodes/install.sh
mkdir -p /workspace/quantumcodes
cd /workspace/quantumcodes
git clone git@gitlab.com:quantumcodes/rpm_agent
git clone git@gitlab.com:quantumcodes/rpm_agent_monitor
git clone git@gitlab.com:quantumcodes/rpm_script_generator
git clone git@gitlab.com:quantumcodes/rpm_script_recorder
