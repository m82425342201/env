sh /workspace/ssh-keys/dev.gridone/install.sh
mkdir -p /workspace/dev.gridone
cd /workspace/dev.gridone
git clone git@gitlab.com:dev.gridone/perfone-gs2
git clone git@gitlab.com:dev.gridone/perfone-controller-ver3
git clone git@gitlab.com:dev.gridone/perfone-pro-analyser
git clone git@gitlab.com:dev.gridone/perfone-erlang
git clone git@gitlab.com:dev.gridone/perfone-resource-agent
git clone git@gitlab.com:dev.gridone/perfone-result-uploader
git clone git@gitlab.com:dev.gridone/perfone-exec-agent-dotnet
