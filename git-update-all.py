from pathlib import Path

for d in Path(".").glob("*"):
    if (d / ".git").is_dir():
        print(f"echo [{d}]")
        print(f"cd {d}")
        print(f"git pull")
        print(f"cd ..")
